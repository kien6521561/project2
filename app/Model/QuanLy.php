<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuanLy extends Model
{
    protected $table = 'quan_ly';
    protected $fillable =[
    	'ten',
    'email',
    'mat_khau',
    'anh',
    'cap_do'


    ];

    public $timestamps = false;
    protected $primaryKey = 'ma_quan_ly';
    
}