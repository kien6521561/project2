<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nghi extends Model
{
    protected $table = 'nghi';
   
    public $timestamps = false;
    protected $primaryKey = ['ma_nhan_vien','ngay_nghi'];
}