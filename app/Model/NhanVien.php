<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NhanVien extends Model
{
    protected $table = 'nhan_vien';
    protected $fillable = [
    	'ten',
    	'gioi_tinh',
    	'ngay_sinh',
    	'email',
    	'mat_khau',
    	'luong_co_ban',
        'ma_phong_ban'
    
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';

    public function phongban(){

        return $this->belongsTo('App\Model\PhongBan', 'ma_phong_ban' );
    }
    public function getTenGioiTinhAttribute()
    {
        if ($this->gioi_tinh==1) {
            return "Nam";
        } else {
            return "Nữ";
        }
        
    }
     public function getTuoiAttribute()
    {
        $age = date_diff(date_create($this->ngay_sinh), date_create('now'))->y;
        return $age;
    }
    public function thoigian(){
        $now = DateTime();
        $timestamp = $now->getTimestamp(); 
    }
}
