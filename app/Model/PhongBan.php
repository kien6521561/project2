<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PhongBan extends Model
{
    protected $table = 'phong_ban';
    protected $fillable = [
    	'ten'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
