<?php

namespace App\Imports;

use App\Model\NhanVien;
use App\Model\PhongBan;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class NhanVienImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $row = array_filter($row);
        if(!empty($row)){
             $array = [
            'ten' => $row['nhan_vien'],
            'ngay_sinh' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['ngay_sinh'])->format('Y-m-d'),
            'gioi_tinh' => ($row['gioi_tinh']=='Nam') ? 1 :0,
            'ma_phong_ban' => PhongBan::firstOrCreate(['ten' => $row['phong_ban']])->ma,
        ];
        return new NhanVien($array);
        }
       
    }
}
