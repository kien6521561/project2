<?php
namespace App\Http\Controllers;
use Exception;
use Illuminate\Http\Request;
use App\Model\QuanLy;
use App\Model\Nhanvien;

use Session;
use Storage;

class Controller{
	public function master(){
		return view('layout.master');
	}
	public function login(){
		return view('view_login');
	}
	public function process_login(Request $rq)
	{
		try {
			$quan_ly = QuanLy::where('email',$rq->email)
		->where('mat_khau',$rq->mat_khau)
		->firstOrFail();
			
		} catch (Exception $e) {
			return redirect()->route('view_login')->with('error','đăng nhập thất bại');
		}
		
		Session::put('ma_quan_ly',$quan_ly->ma_quan_ly);
		Session::put('ten',$quan_ly->ten);
		Session::put('anh',$quan_ly->anh);
		Session::put('cap_do',$quan_ly->cap_do);
		return redirect('');
}
	public function view_upload_avatar(){
		return view('view_upload_avatar');
	}
	public function process_upload_avatar(Request $rq){
		$link = Storage::disk('public')->put('avatars', $rq->anh);
		QuanLy::where('ma_quan_ly',Session::get('ma_quan_ly'))
		->update(
			[
				'anh' => $link
			]);
		Session::put('anh', $link);
	}
	public function login_nhan_vien(){
		return view('view_login_nhan_vien');
	}
	public function process_login_nhan_vien(Request $rq)
	{
		try {
			$nhan_vien = NhanVien::where('email',$rq->email)
			->where('mat_khau',$rq->mat_khau)
			->firstOrFail();
			
		} catch (Exception $e) {
			return redirect()->route('view_login_nhan_vien')->with('error1','đăng nhập thất bại');
		}
		
		Session::put('ma_nhan_vien',$nhan_vien->ma);
		Session::put('ten',$nhan_vien->ten);
	
		return redirect()->route('cham_cong.welcome');
		
	}
	public function logout_nhan_vien()
    {
        Session::flush();
        return redirect() -> route('view_login_nhan_vien');
    }
		

}