<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Nghi;
use App\Model\NhanVien;
use Session;

class NghiController{

	public function view_nghi()
	{
		$ma_nhan_vien = Session::get('ma_nhan_vien');
		
		$nghi = Nghi::where('ma_nhan_vien',$ma_nhan_vien)
		->select()
		->first();
		return view('nhan_vien.view_nghi',compact('nghi'));
	}
	public function process_nghi(Request $rq){
		$ma=Session::get('ma_nhan_vien');
      	
         Nghi::insert([
        'ngay_nghi'=>$rq -> ngay_nghi,
        'ma_nhan_vien'=>$ma,
        
        ]);
        return redirect()->route('cham_cong.view_nghi');
	}
}