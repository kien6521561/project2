<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PhongBan;
use App\Model\NhanVien;
use App\Model\ChamCong;
use App\Imports\NhanVienImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;




class NhanVienController 
{
   public function view_all()
   {
        $array_nhan_vien = NhanVien:: with('phongban')-> get();
        return view('nhan_vien.view_all',compact('array_nhan_vien'));
   }
    public function view_insert()
    
    {
        $array_phong_ban = PhongBan::get();
        return view('nhan_vien.view_insert',compact('array_phong_ban'));
    }
    public function process_insert(Request $rq)
    {
       
        NhanVien::create($rq->all());
        return redirect()->route('nhan_vien.view_all');
    }
   public function view_update($ma){
    $nhan_vien = NhanVien::where('ma','=',$ma)->first();
    $array_phong_ban = PhongBan::get();
    //$nhan_vien = NhanVien::find($ma);
    return view('nhan_vien.view_update', compact('nhan_vien','array_phong_ban'));
   }
   public function process_update($ma, Request $rq)
  {
    NhanVien::find($ma)->update($rq->all());

    return redirect()->route('nhan_vien.view_all');
  }

   public function delete($ma){

    NhanVien::find($ma)->delete();
      return redirect()->route('nhan_vien.view_all');
  }
  public function view_nhan_vien_import(){
    return view('nhan_vien.view_nhan_vien_import');
  }

  public function procces_nhan_vien_import(Request $rq){
     Excel::import(new NhanVienImport, $rq->file_nhan_vien);
     return redirect()->route('nhan_vien.view_all');
  }
  public function view_all_cong()
  {
     $ma_nhan_vien = Session::get('ma_nhan_vien');
     $array_nhan_vien = ChamCong::join('nhan_vien','nhan_vien.ma','cham_cong.ma_nhan_vien')
     -> select('cham_cong.*','nhan_vien.ten')
     -> selectRaw('TIMEDIFF(check_out,check_in) as tong_gio')
     -> get();
    return view('nhan_vien.view_all_cong',compact('array_nhan_vien'));
  }

}
