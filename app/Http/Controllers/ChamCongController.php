<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ChamCong;
use App\Model\NhanVien;
use Session;

class ChamCongController{

	public function welcome()
	{
		$ma_nhan_vien = Session::get('ma_nhan_vien');
		$ngay_hien_tai = date('Y-m-d');
		$cham_cong = ChamCong::where('ma_nhan_vien',$ma_nhan_vien)
		->where('ngay',$ngay_hien_tai)
		->select()
		->selectRaw('TIMEDIFF(check_out,check_in) as tong_gio')
		->first();
		return view('nhan_vien.welcome',compact('cham_cong'));
	}
	public function check_in(){
        $ma=Session::get('ma_nhan_vien');
        $ngay=date('Y-m-d');
        $gio=date('H:i');
        ChamCong::insert([
        'ngay'=>$ngay,
        'ma_nhan_vien'=>$ma,
        'check_in'=>$gio,
        ]);
        return redirect()->route('cham_cong.welcome');
    }
    public function check_out(){
        $ma=session()->get('ma_nhan_vien');
        $ngay=date('Y-m-d');
        $gio=date('H:i');
        ChamCong::where([
            'ngay'=>$ngay,
            'ma_nhan_vien'=>$ma,
        ])->update([
            'check_out'=>$gio,
        ]);
        return redirect()->route('cham_cong.welcome');
    }

	
	
}
