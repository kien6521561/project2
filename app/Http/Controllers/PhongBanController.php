<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PhongBan;

class PhongBanController 
{
   public function view_all()
   {
        $array_phong_ban = PhongBan::get();
        return view('phong_ban.view_all',compact('array_phong_ban'));
   }
    public function view_insert()
    
    {

        return view('phong_ban.view_insert');
    }
    public function process_insert(Request $rq)
    {
       
        PhongBan::create($rq->all());
        return redirect()->route('phong_ban.view_all');
    }
   public function view_update($ma){
    $phong_ban = PhongBan::where('ma','=',$ma)->first();
    //$phong_ban = PhongBan::find($ma);
    return view('phong_ban.view_update', compact('phong_ban'));
   }
   public function process_update($ma, Request $rq)
  {
    PhongBan::find($ma)->update($rq->all());

    return redirect()->route('phong_ban.view_all');
  }

   public function delete($ma){

    PhongBan::find($ma)->delete();
      return redirect()->route('phong_ban.view_all');
}
}
