@extends('layout.master')

@section('content')

<form action="{{ route('nhan_vien.process_insert') }}" method="post">
	{{ csrf_field() }}
	Tên nhân viên
	<input type="text" name="ten">
	<br>  
	Giới tính
	<input type="radio" name="gioi_tinh" value="1">Nam
	<input type="radio" name="gioi_tinh" value="0">Nữ
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh">
	<br>
	Email
	<input type="text" name="email">
	<br>
	Mật khẩu
	<input type="password" name="mat_khau">
	<br>
	Lương cơ bản
	<input type="text" name="luong_co_ban">
	<br>
	
	Phòng ban
	<select name="ma_phong_ban">
		@foreach($array_phong_ban as $phong_ban)
		<option value="{{ $phong_ban->ma }}">
			{{ $phong_ban->ten }}
		</option>
		@endforeach
		
	</select>



	
<br>
	<button>Thêm</button>
</form>
@endsection