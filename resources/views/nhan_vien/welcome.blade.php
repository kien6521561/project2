@extends('layout.master')

@section('content')
<h1>Chấm công cho nhân viên</h1>

@if(isset($cham_cong))
	@if(isset($cham_cong->check_out))
		{{ $cham_cong->tong_gio }}
	@else
		<a href="{{ route('cham_cong.check_out') }}">
			Check out
		</a>
	@endif
@else
	<a href="{{ route('cham_cong.check_in') }}">
		Check in
	</a>
@endif
@endsection