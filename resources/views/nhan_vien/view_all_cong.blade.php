@extends('layout.master')

@section('content')
<h1>Danh sách chấm công nhân viên</h1>
<a href="{{ route('nhan_vien.view_insert') }}">Thêm</a>
<table border="1"width="60%">
	<tr>
		<th>ho ten</th>
		<th>ngay  </th>
		<th>tong so gio</th>	
	</tr>
	@foreach ($array_nhan_vien as $nhan_vien)
	<tr>
		<td>
			{{ $nhan_vien-> ten }}
		</td>
		<td>
			{{ $nhan_vien -> ngay }}
		</td>
		<td>{{ $nhan_vien -> tong_gio }}</td>
	</tr>
	@endforeach
</table>
@endsection