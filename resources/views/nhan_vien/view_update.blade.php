@extends('layout.master')

@section('content')
<form action="{{ route('nhan_vien.process_update',['ma' => $nhan_vien->ma]) }}" method="post">
	{{ csrf_field() }}
	Tên
	<input type="text" name="ten" value="{{ $nhan_vien->ten }}">
	<br>  
	Giới tính
	<input type="radio" name="gioi_tinh" value="1"
		@if($nhan_vien->gioi_tinh==1)
			checked
		@endif
	>Nam
	<input type="radio" name="gioi_tinh" value="0"
		@if($nhan_vien->gioi_tinh==0)
			checked
		@endif
	>Nữ
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh" value="{{ $nhan_vien->ngay_sinh }}">
	<br>
	Email
	<input type="text" name="email" value="{{ $nhan_vien->email }}">
	<br>
	Mật khẩu
	<input type="password" name="mat_khau" value="{{ $nhan_vien->mat_khau }}">
	<br>
	Lương cơ bản
	<input type="text" name="luong_co_ban" value="{{ $nhan_vien->luong_co_ban }}">
	<br>
	
	Phòng ban
	<select name="ma_phong_ban">
		@foreach($array_phong_ban as $phong_ban)
		<option value="{{ $phong_ban->ma }}"
			@if($nhan_vien->ma_phong_ban==$phong_ban->ma)
			selected
		@endif
			>
			{{ $phong_ban->ten }}
		</option>
		@endforeach
		
	</select>



	
<br>
	

	
	<button>Sửa</button>
</form>
@endsection