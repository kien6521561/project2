

<!DOCTYPE html>
<html>

<head>
    <!-- Meta and Title -->
    <meta charset="utf-8">
    <title>AdminK - A Responsive Bootstrap 3 Admin Dashboard Template</title>
    <meta name="keywords" content="HTML5, Bootstrap 3, Admin Template, UI Theme"/>
    <meta name="description" content="AdminK - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="ThemeREX">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- Angular material -->
    <link rel="stylesheet" type="text/css" href="{{ asset('skin/css/angular-material.min.css')}}">
    
    <!-- Icomoon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/icomoon/icomoon.css')}}">    
    
    <!-- AnimatedSVGIcons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/animatedsvgicons/css/codropsicons.css')}}">


    

    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/plugins/magnific/magnific-popup.css')}}">

    <!-- c3charts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/plugins/c3charts/c3.min.css')}}">



    <!-- CSS - allcp forms -->
    <link rel="stylesheet" type="text/css" href="{{ asset('allcp/forms/css/forms.css')}}">

    <!-- mCustomScrollbar -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css')}}">

    <!-- CSS - theme -->
    <link rel="stylesheet" type="text/css" href="{{ asset('skin/default_skin/less/theme.css')}}">
    
    <!-- IE8 HTML5 support -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="utility-page sb-l-c sb-r-c">

<!-- Body Wrap -->
<div id="main" class="animated fadeIn">

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <div id="canvas-wrapper">
            <canvas id="demo-canvas"></canvas>
        </div>

        <!-- Content -->
        <section id="content">

            <!-- Login Form -->
            <div class="allcp-form theme-primary mw320" id="login">
                <div class="bg-primary mw600 text-center mb20 br3 pt15 pb10">
                    <img src="assets/img/logo.png" alt=""/>
                </div>
                @if(Session::has('error1'))
                    <h2>
                        {{ Session::get('error1') }}
                    </h2>
                @endif
                <div class="panel mw320">

                    <form method="post" id="form-login" action="{{ route('process_login_nhan_vien') }}">
                        {{ csrf_field() }}
                        <div class="panel-body pn mv10">

                            <div class="section">
                                <label for="username" class="field prepend-icon">
                                    <input type="email" name="email" id="username" class="gui-input"
                                           placeholder="Username">
                                    <span class="field-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </label>
                            </div>
                            <!-- /section -->

                            <div class="section">
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="mat_khau" id="password" class="gui-input"
                                           placeholder="Password">
                                    <span class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </label>
                            </div>
                            <!-- /section -->

                            <div class="section">
                                <div class="bs-component pull-left pt5">
                                    <div class="radio-custom radio-primary mb5 lh25">
                                        <input type="radio" id="remember" name="remember">
                                        <label for="remember">Remember me</label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-bordered btn-primary pull-right">Log in</button>
                            </div>
                            <!-- /section -->

                        </div>
                        <!-- /Form -->
                    </form>
                </div>
                <!-- /Panel -->
            </div>
            <!-- /Spec Form -->

        </section>
        <!-- /Content -->

    </section>
    <!-- /Main Wrapper -->

</div>
<!-- /Body Wrap  -->



<!-- Scripts -->

<!-- jQuery -->
<script src="{{ asset('js/jquery/jquery-1.12.3.min.js')}}"></script>
<script src="{{ asset('js/jquery/jquery_ui/jquery-ui.min.js')}}"></script>

<!-- AnimatedSVGIcons -->
<script src="{{ asset('fonts/animatedsvgicons/js/snap.svg-min.js')}}"></script>
<script src="{{ asset('fonts/animatedsvgicons/js/svgicons-config.js')}}"></script>
<script src="{{ asset('fonts/animatedsvgicons/js/svgicons.js')}}"></script>
<script src="{{ asset('fonts/animatedsvgicons/js/svgicons-init.js')}}"></script>

<!-- Scroll -->
<script src="{{ asset('js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js')}}"></script>

<script src="{{ asset('js/plugins/highcharts/highcharts.js')}}"></script>

<script src="{{ asset('js/plugins/canvasbg/canvasbg.js')}}"></script>

<!-- Theme Scripts -->
<script src="{{ asset('js/utility/utility.js')}}"></script>
<script src="{{ asset('js/demo/demo.js')}}"></script>
<script src="{{ asset('js/main.js')}}"></script>
<script src="{{ asset('js/demo/widgets_sidebar.js')}}"></script>
<script src="assets/js/pages/dashboard_init.js"></script>

<!-- /Scripts -->

</body>

</html>


