<!-- Sidebar  -->
    <aside id="sidebar_left" class="nano affix">

        <!-- Sidebar Left Wrapper  -->
        <div class="sidebar-left-content nano-content">

            <!-- Sidebar Header -->
            <header class="sidebar-header">

                <!-- Sidebar - Logo -->
                <div class="sidebar-widget logo-widget">
                    <div class="media">
                        <a class="logo-image" href="index.html">
                            <img src="assets/img/logo.png" alt="" class="img-responsive">
                        </a>

                        <div class="logo-slogan">
                            <div><span class="text-info">Phần mềm chấm công</span></div>
                        </div>
                    </div>
                </div>

            </header>
            <!-- /Sidebar Header -->

            <!-- Sidebar Menu  -->
            <ul class="nav sidebar-menu">
                <li class="sidebar-label pt30">Navigation</li>
                <li>
                    <a class="accordion-toggle menu-open" href="#">
                        <span class="caret"></span>
                        <span class="sidebar-title">Dashboard</span>
                        <span class="sb-menu-icon fa fa-home"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li class="active">
                            <a href="{{ route('phong_ban.view_all') }}">
                                Phòng bàn 
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('nhan_vien.view_all') }}">
                                Nhân viên
                            </a>
                        </li>
                         <li>
                            <a href="{{ route('nhan_vien.view_nhan_vien_import') }}">
                               Import Excel
                            </a>
                        </li>
                      <li>
                            <a href="{{ route('cham_cong.view_nghi') }}">
                               Đăng kí nghỉ
                            </a>
                        </li>
                         <li>
                            <a href="{{ route('nhan_vien.view_all_cong') }}">
                               Quản lý chấm công
                                
                            </a>
                        </li>
                         
                    </ul>
            

            </ul>
            <!-- /Sidebar Menu  -->

        </div>
        <!-- /Sidebar Left Wrapper  -->

    </aside>
    <!-- /Sidebar -->
    