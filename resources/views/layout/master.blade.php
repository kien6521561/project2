
<!DOCTYPE html>
<html>

<head>
    <!-- Meta and Title -->
    <meta charset="utf-8">
    <title>do_an2</title>
    <meta name="keywords" content="HTML5, Bootstrap 3, Admin Template, UI Theme"/>
    <meta name="description" content="AdminK - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="ThemeREX">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- Angular material -->
    <link rel="stylesheet" type="text/css" href="{{ asset('skin/css/angular-material.min.css')}}">
    
    <!-- Icomoon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/icomoon/icomoon.css')}}">    
    
    <!-- AnimatedSVGIcons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/animatedsvgicons/css/codropsicons.css')}}">


    

    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/plugins/magnific/magnific-popup.css')}}">

    <!-- c3charts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/plugins/c3charts/c3.min.css')}}">



    <!-- CSS - allcp forms -->
    <link rel="stylesheet" type="text/css" href="{{ asset('allcp/forms/css/forms.css')}}">

    <!-- mCustomScrollbar -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css')}}">

    <!-- CSS - theme -->
    <link rel="stylesheet" type="text/css" href="{{ asset('skin/default_skin/less/theme.css')}}">


</head>

<body class="dashboard-page with-customizer">

<!-- Body Wrap  -->
<div id="main">

    @include('layout.header')

    @include('layout.sidebar')
    <!-- Main Wrapper -->
    <section id="content_wrapper">

                <!-- Topbar Menu Wrapper -->
    @include('layout.topbar')
       

        <div class="greeting-field">
          
            
        </div>

        <!-- Content -->@yield('content')
        <section id="content" class="table-layout animated fadeIn">

            <!-- Column Center -->
            <div class="chute chute-center">

                <!-- AllCP Info -->
                <div class="allcp-panels fade-onload">

                    <div class="row">
                        <!-- AllCP Grid -->
                        <div class="col-md-12 allcp-grid">
                            
                                                      
                    </div>




                    </div>


                </div>


            </div>
            <!-- /Column Center -->

        </section>
        <!-- /Content -->

        

    </section>
    <!-- /Main Wrapper -->

</div>
<!-- /Body Wrap  -->

<!-- Scripts -->

<!-- jQuery -->
<script src="{{ asset('js/jquery/jquery-1.12.3.min.js')}}"></script>
<script src="{{ asset('js/jquery/jquery_ui/jquery-ui.min.js')}}"></script>

<!-- AnimatedSVGIcons -->
<script src="{{ asset('fonts/animatedsvgicons/js/snap.svg-min.js')}}"></script>
<script src="{{ asset('fonts/animatedsvgicons/js/svgicons-config.js')}}"></script>
<script src="{{ asset('fonts/animatedsvgicons/js/svgicons.js')}}"></script>
<script src="{{ asset('fonts/animatedsvgicons/js/svgicons-init.js')}}"></script>

<!-- Scroll -->
<script src="{{ asset('js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js')}}"></script>

<!-- Mixitup -->


<!-- Summernote -->



<!-- HighCharts Plugin -->
<script src="{{ asset('js/plugins/highcharts/highcharts.js')}}"></script>

<!-- Highlight JS -->


<!-- Date/Month - Pickers -->





<!-- Magnific Popup Plugin -->
<script src="{{ asset('js/plugins/magnific/jquery.magnific-popup.js')}}"></script>

<!-- FullCalendar Plugin -->

')}}

<!-- Plugins -->


<script src="{{ asset('js/plugins/c3charts/d3.min.js')}}"></script>
<script src="{{ asset('js/plugins/c3charts/c3.min.js')}}"></script>





<script src="{{ asset('js/plugins/circles/circles.js')}}"></script>




<!-- Google Map API -->





    
<!-- Theme Scripts -->
<script src="{{ asset('js/utility/utility.js')}}"></script>
<script src="{{ asset('js/demo/demo.js')}}"></script>
<script src="{{ asset('js/main.js')}}"></script>
<script src="{{ asset('js/demo/widgets_sidebar.js')}}"></script>

<script src="{{ asset('js/pages/dashboard1.js')}}"></script>

<script src="{{ asset('js/demo/widgets.js')}}"></script>




<!-- /Scripts -->

</body>

</html>


