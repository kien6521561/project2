@extends('layout.master')

@section('content')

<h1>Danh sách phòng ban</h1>
<a href="{{ route('phong_ban.view_insert') }}">Thêm</a>
<table border="1"width="60%">
	<tr>
		<th>Mã</th>
		<th>Tên phòng ban</th>
		
		<th>Sửa</th>
		<th>Xóa</th>
	</tr>
	@foreach ($array_phong_ban as $phong_ban)
	<tr>
		<td>
			{{ $phong_ban->ma }}
		</td>
		<td>
			{{ $phong_ban->ten }}
		</td>
		
		<td>
		<a href="{{ route('phong_ban.view_update',['ma'=> $phong_ban -> ma]) }}">
			Sửa
		</a>	
		</td>
		<td>
		<a href="{{ route('phong_ban.delete',['ma'=> $phong_ban -> ma]) }}">
			xóa
		</a>	
		</td>
	</tr>
	@endforeach
</table>
@endsection