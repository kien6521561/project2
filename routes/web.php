<?php

Route::get('logout_nhan_vien','Controller@logout_nhan_vien')->name('logout_nhan_vien');

Route::group(['middleware' => 'CheckLogin'],function(){
	
	Route::group(['prefix' => 'phong_ban','as' =>'phong_ban.'],function(){
		Route::get('','PhongBanController@view_all')->name('view_all');
		Route::get('view_insert','PhongBanController@view_insert')->name('view_insert');
		Route::post('process_insert','PhongBanController@process_insert')->name('process_insert');

		Route::get('view_update/{ma}','PhongBanController@view_update')->name('view_update');
		Route::post('process_update/{ma}','PhongBanController@process_update')->name('process_update');
		Route::get('delete/{ma}','PhongBanController@delete')->name('delete');



});
Route::group(['prefix' => 'nhan_vien','as' =>'nhan_vien.'],function(){
		Route::get('','NhanVienController@view_all')->name('view_all');
		Route::get('view_insert','NhanVienController@view_insert')->name('view_insert');
		Route::post('process_insert','NhanVienController@process_insert')->name('process_insert');

		Route::get('view_update/{ma}','NhanVienController@view_update')->name('view_update');
		Route::post('process_update/{ma}','NhanVienController@process_update')->name('process_update');
		Route::get('delete/{ma}','NhanVienController@delete')->name('delete');
Route::get('view_nhan_vien_import','NhanVienController@view_nhan_vien_import')->name('view_nhan_vien_import');
Route::post('procces_nhan_vien_import','NhanVienController@procces_nhan_vien_import')->name('procces_nhan_vien_import');
Route::get('view_all_cong','NhanVienController@view_all_cong')->name('view_all_cong');
		

});

	Route::get('view_upload_avatar','Controller@view_upload_avatar')->name('view_upload_avatar');
		Route::post('process_upload_avatar','Controller@process_upload_avatar')->name('process_upload_avatar');



});


Route::get('','Controller@master');
Route::get('view_login','Controller@login')->name('view_login');
Route::post('process_login','Controller@process_login')->name('process_login');
Route::get('view_login_nhan_vien','Controller@login_nhan_vien')->name('view_login_nhan_vien');
Route::post('process_login_nhan_vien','Controller@process_login_nhan_vien')->name('process_login_nhan_vien');


Route::group(['middleware' => 'CheckLogin_nhan_vien'],function(){

	Route::group(['prefix' => 'cham_cong','as' =>'cham_cong.'],function(){
		
		Route::get('welcome', 'ChamCongController@welcome')->name('welcome');
        Route::get('check_in', 'ChamCongController@check_in')->name('check_in');
        Route::get('check_out', 'ChamCongController@check_out')->name('check_out');
        Route::get('view_nghi', 'NghiController@view_nghi')->name('view_nghi');
        Route::post('process_nghi', 'NghiController@process_nghi')->name('process_nghi');
        


});

});









